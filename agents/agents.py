import numpy as np

class epsGreedyAgent:
    def __init__(self, A, epsilon):
        self.epsilon = epsilon
        self.A = A
        self.cumvalue = np.zeros(A)
        self.numpick = np.zeros(A)

    def interact(self):
        rand = np.random.uniform()
        if rand < self.epsilon:
            a = np.random.randint(0, self.A)
        else:
            value = [c / (n > 0 and n or 1) for c, n
                     in zip(self.cumvalue, self.numpick)]
            a = np.argmax(value)
        return a

    def update(self, a, r):
        self.numpick[a] += 1
        self.cumvalue[a] += r

class optimistEpsGreedyAgent:
    def __init__(self, A, epsilon, optimism):
        # TO IMPLEMENT
        raise NameError('optimistEpsGreedyAgent')
        return

    def interact(self):
        # TO IMPLEMENT
        return

    def update(self, a, r):
        # TO IMPLEMENT
        return

class softmaxAgent:
    def __init__(self, A, temperature):
        # TO IMPLEMENT
        raise NameError('softmaxAgent')
        return

    def interact(self):
        # TO IMPLEMENT
        return 

    def update(self, a, r):
        # TO IMPLEMENT
        return

class UCBAgent:
    def __init__(self, A):
        # TO IMPLEMENT
        raise NameError('UCBAgent')
        return

    def interact(self):
        # TO IMPLEMENT
        return

    def update(self, a, r):
        # TO IMPLEMENT
        return

class ThompsonAgent:
    def __init__(self, A, mu_0, var_0):
        # TO IMPLEMENT
        raise NameError('ThompsonAgent')
        return

    def interact(self):
        # TO IMPLEMENT
        return

    def update(self, a, r):
        # TO IMPLEMENT
        return
